<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /*function store(Request $request){
        $books = new Books();
        $books->setAttribute("name",$request->name);
        $books->setAttribute("category",$request->category);
        $books->setAttribute("description",$request->description);
        if($books->save()){
            return true;
        }
    }*/
    function store(Request $request){
        $books = new Books();
        if (Books::Create($request->all())){
            return true;
        }
    }

    function update(Request $request, Books $books){
        if ($books->fill($request->all())->save()){
            return true;
        }
    }
    function index(){
        $books = Books::all();
        return $books;
    }
    function show(Books $books){
        return $books;
    }
    function destroy(Books $books){
        if ($books->delete()){
            return true;
        }
    }
}
